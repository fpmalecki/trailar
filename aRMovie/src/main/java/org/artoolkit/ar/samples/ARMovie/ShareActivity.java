package org.artoolkit.ar.samples.ARMovie;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareButton;

import org.artoolkit.ar.base.camera.CameraPreferencesActivity;

public class ShareActivity extends FragmentActivity {

    CallbackManager cbm;
    AccessToken at;
    Profile p;
    ProfileTracker pt;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_share);

        View someView = findViewById(R.id.activity_share);
        View root = someView.getRootView();
        root.setBackgroundColor(getResources().getColor(android.R.color.white));

        tv = (TextView) findViewById(R.id.textView);

        cbm = CallbackManager.Factory.create();

        LoginButton lb = (LoginButton) findViewById(R.id.login_button);
        lb.registerCallback(cbm, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                at = loginResult.getAccessToken();
                pt = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        pt.stopTracking();
                        p = Profile.getCurrentProfile();
                        tv.setText("Witaj, " + p.getFirstName() + " " + p.getLastName());
                    }
                };
                pt.startTracking();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Logging in canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getApplicationContext(), "Error while logging in", Toast.LENGTH_SHORT).show();
            }
        });

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://www.youtube.com/watch?v=HSzx-zryEgM"))
                .build();

        ShareButton shareButton = (ShareButton) findViewById(R.id.share_button);
        shareButton.setShareContent(content);

        LikeView likeView = (LikeView) findViewById(R.id.like_view);
        likeView.setObjectIdAndType("https://www.youtube.com/watch?v=HSzx-zryEgM", LikeView.ObjectType.PAGE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            startActivity(new Intent(this, CameraPreferencesActivity.class));
            return true;
        } else if (item.getItemId() == R.id.gmaps) {
            startActivity(new Intent(this, MapsActivity.class));
            return true;
        } else if (item.getItemId() == R.id.fbshare) {
            startActivity(new Intent(this, ShareActivity.class));
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
